
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
	<title>{{ $title }}</title>
  <style type="text/css">
    table tr td {
      border:1px solid gray;
      padding: 5px;
    }
  </style>
</head>
<body>
	<h1>{{ $title }}</h1>
	<table>
    <tbody>
      <tr>
        <td style="width:20%;">Reference Number</td>
        <td style="width:80%;">{{ $record->reference_number }}</td>
      </tr>
      <tr>
        <td>Scheduled Date</td>
        <td>{{ $record->schedule_date }}</td>
      </tr>
      <tr>
        <td>Scheduled Time</td>
        <td>{{ $record->time_range->name }}</td>
      </tr>
      <tr>
        <td>Charges</td>
        <td>RM 3.50</td>
      </tr>
    </tbody>
</body>
</html>